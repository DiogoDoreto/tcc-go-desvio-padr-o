package main

import (
	"fmt"
	"math"
	"math/rand"
	"runtime"
	"time"
)

const N = 100000000

func Sum(v []float64) float64 {
	sum := v[0]
	for _, num := range v[1:] {
		sum += num
	}
	return sum
}

func splitWork(v []float64, ch chan []float64, workers int) {
	remWorkers := workers
	remItems := N //len(v)
	var slice []float64

	for remWorkers > 0 {
		size := remItems / remWorkers
		slice, v = v[:size], v[size:]
		remWorkers--
		remItems -= size

		ch <- slice
	}
}

func main() {
	workers := runtime.GOMAXPROCS(0)
	values := make([]float64, N)

	for i := range values {
		values[i] = rand.Float64()
	}

	work := make(chan []float64, workers)
	res := make(chan float64, workers)

	start := time.Now()
	go splitWork(values, work, workers)

	for i := 0; i < workers; i++ {
		go func(res chan<- float64, work <-chan []float64) {
			res <- Sum(<-work)
		}(res, work)
	}

	sum_v := 0.0
	for i := 0; i < workers; i++ {
		sum_v += <-res
	}

	N := float64(len(values))
	mean_v := sum_v / N

	go splitWork(values, work, workers)
	for i := 0; i < workers; i++ {
		go func(mean_v float64, res chan<- float64, work <-chan []float64) {
			sum := 0.0
			for _, v := range <-work {
				d := v - mean_v
				sum += d * d
			}
			res <- sum
		}(mean_v, res, work)
	}

	desv := 0.0
	for i := 0; i < workers; i++ {
		desv += <-res
	}
	desv = math.Sqrt(desv / N)
	duration := time.Since(start)

	fmt.Println("s =", desv)
	fmt.Println("Duration =", duration.Seconds()*1000)
}
